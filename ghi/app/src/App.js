import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <div className="container">
        <LocationForm /> 
      {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </>
  );
}

export default App;